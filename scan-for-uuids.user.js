// ==UserScript==
// @name         Scan for UUIDs
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Add copy links to all any hyperlinks that conatin a UUID
// @author       coling87
// @match        https://*/*
// @run-at       context-menu
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    // eg. #em/eventAdmin,;eventadmin,id=0b11bbfe-a482-44e9-ba03-b6b608a19e4e,tab=REGISTERS,;
    let re = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/i;

    let copyImageSrc = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSIwIDAgMTE1Ljc3IDEyMi44OCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMTE1Ljc3IDEyMi44OCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+PHN0eWxlIHR5cGU9InRleHQvY3NzIj4uc3Qwe2ZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO308L3N0eWxlPjxnPjxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik04OS42MiwxMy45NnY3LjczaDEyLjE5aDAuMDF2MC4wMmMzLjg1LDAuMDEsNy4zNCwxLjU3LDkuODYsNC4xYzIuNSwyLjUxLDQuMDYsNS45OCw0LjA3LDkuODJoMC4wMnYwLjAyIHY3My4yN3YwLjAxaC0wLjAyYy0wLjAxLDMuODQtMS41Nyw3LjMzLTQuMSw5Ljg2Yy0yLjUxLDIuNS01Ljk4LDQuMDYtOS44Miw0LjA3djAuMDJoLTAuMDJoLTYxLjdINDAuMXYtMC4wMiBjLTMuODQtMC4wMS03LjM0LTEuNTctOS44Ni00LjFjLTIuNS0yLjUxLTQuMDYtNS45OC00LjA3LTkuODJoLTAuMDJ2LTAuMDJWOTIuNTFIMTMuOTZoLTAuMDF2LTAuMDJjLTMuODQtMC4wMS03LjM0LTEuNTctOS44Ni00LjEgYy0yLjUtMi41MS00LjA2LTUuOTgtNC4wNy05LjgySDB2LTAuMDJWMTMuOTZ2LTAuMDFoMC4wMmMwLjAxLTMuODUsMS41OC03LjM0LDQuMS05Ljg2YzIuNTEtMi41LDUuOTgtNC4wNiw5LjgyLTQuMDdWMGgwLjAyaDYxLjcgaDAuMDF2MC4wMmMzLjg1LDAuMDEsNy4zNCwxLjU3LDkuODYsNC4xYzIuNSwyLjUxLDQuMDYsNS45OCw0LjA3LDkuODJoMC4wMlYxMy45Nkw4OS42MiwxMy45NnogTTc5LjA0LDIxLjY5di03Ljczdi0wLjAyaDAuMDIgYzAtMC45MS0wLjM5LTEuNzUtMS4wMS0yLjM3Yy0wLjYxLTAuNjEtMS40Ni0xLTIuMzctMXYwLjAyaC0wLjAxaC02MS43aC0wLjAydi0wLjAyYy0wLjkxLDAtMS43NSwwLjM5LTIuMzcsMS4wMSBjLTAuNjEsMC42MS0xLDEuNDYtMSwyLjM3aDAuMDJ2MC4wMXY2NC41OXYwLjAyaC0wLjAyYzAsMC45MSwwLjM5LDEuNzUsMS4wMSwyLjM3YzAuNjEsMC42MSwxLjQ2LDEsMi4zNywxdi0wLjAyaDAuMDFoMTIuMTlWMzUuNjUgdi0wLjAxaDAuMDJjMC4wMS0zLjg1LDEuNTgtNy4zNCw0LjEtOS44NmMyLjUxLTIuNSw1Ljk4LTQuMDYsOS44Mi00LjA3di0wLjAyaDAuMDJINzkuMDRMNzkuMDQsMjEuNjl6IE0xMDUuMTgsMTA4LjkyVjM1LjY1di0wLjAyIGgwLjAyYzAtMC45MS0wLjM5LTEuNzUtMS4wMS0yLjM3Yy0wLjYxLTAuNjEtMS40Ni0xLTIuMzctMXYwLjAyaC0wLjAxaC02MS43aC0wLjAydi0wLjAyYy0wLjkxLDAtMS43NSwwLjM5LTIuMzcsMS4wMSBjLTAuNjEsMC42MS0xLDEuNDYtMSwyLjM3aDAuMDJ2MC4wMXY3My4yN3YwLjAyaC0wLjAyYzAsMC45MSwwLjM5LDEuNzUsMS4wMSwyLjM3YzAuNjEsMC42MSwxLjQ2LDEsMi4zNywxdi0wLjAyaDAuMDFoNjEuN2gwLjAyIHYwLjAyYzAuOTEsMCwxLjc1LTAuMzksMi4zNy0xLjAxYzAuNjEtMC42MSwxLTEuNDYsMS0yLjM3aC0wLjAyVjEwOC45MkwxMDUuMTgsMTA4LjkyeiIvPjwvZz48L3N2Zz4=";

  function copyToClipBoard(text) {
        var cactive = document.activeElement;

        // create temp text area as content buffer
        var copyBuffer = document.createElement("TEXTAREA");
        document.body.appendChild(copyBuffer);

        copyBuffer.value = text;
        copyBuffer.select();
        copyBuffer.setSelectionRange(0, 99999);
        document.execCommand("copy");

        // restore focus
        if (cactive) {
            cactive.focus();
        }

        // clean up
        document.body.removeChild(copyBuffer);
    }

    function insertLinkAfter(anchor, uuid) {
        let newImage = document.createElement("IMG");
        newImage.setAttribute("src",copyImageSrc);
        newImage.setAttribute("alt",uuid);
        newImage.setAttribute("title",uuid);
        newImage.style.height = "12px";
        newImage.style.paddingLeft = "5px";
        newImage.style.paddingRight = "5px";
        newImage.addEventListener("click", (e) => {
            copyToClipBoard(uuid);
            console.log(uuid);

            let notice=document.createElement("div");
            notice.style.position = "absolute";
            notice.style.left = (e.clientX - 20) + "px";
            notice.style.top = (e.clientY - 20) + "px";
            notice.style.textAlign = "center";
            notice.style.backgroundColor = "#222";
            notice.style.color = "white";
            notice.style.padding = "5px";
            notice.style.fontSize = "12px";
            notice.style.borderRadius = "4px";
            notice.style.opacity = 1;
            notice.appendChild(document.createTextNode("copied to clipboard"));
            notice.style.transition = "opacity 1s linear";
            document.body.appendChild(notice);
            setTimeout(()=>{
                notice.style.opacity = 0;
            }, 10);
            setTimeout(()=>{
                document.body.removeChild(notice);
            }, 1000);
        });
        anchor.parentNode.insertBefore(newImage, anchor.nextSibling);
    }

    document.querySelectorAll("A").forEach(a=>{
        let href = a.getAttribute("href");
        let uuid = href.match(re);
        if (uuid) {
            insertLinkAfter(a, uuid[0]);
        }
    });

})();
